<!DOCTYPE html>
<html>
	<!-- Import des librairies nécessaires -->
	<head>
		<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	</head>
	<body>
		<!-- Template de la page -->			
		<div ng-app="" ng-controller="queryCensusLearnController">
			<div class="well">
				<!-- Formulaire d'interrogation du serveur -->
				<div class="row">
					<div class="col-xs-12 col-md-12">
						<form style="margin-bottom:20px;">
							<select class="form-control" 
								ng-model="selectedAttribute" 
						        ng-options="attribute for attribute in attributeList"
						        ng-change="queryCensusLearn(selectedAttribute);"></select>
					    </form>
					</div>
				</div>
				<!-- Invitation a selectionner un attribut -->
				<div id="results" ng-if="selectedAttribute == NO_ATTRIBUTE_SELECTED">
					<!-- Infos complementaire -->
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="alert alert-info" role="alert">
								<p>Please choose an option to query database.</p>
							</div>
						</div>
					</div>
				</div>
				<!-- Messages d'erreur -->
				<div id="results" ng-if="errorCode!=CODE_NO_ERROR">
					<!-- Infos complementaire -->
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<div class="alert alert-danger" role="alert">
								<p ng-if="errorCode==CODE_ERROR_QUERYING">Oups ! An error occured while querying database.</p>
								<p ng-if="errorCode==CODE_ERROR_FORM">Oups ! An error occured while loading form.</p>
							</div>
						</div>
					</div>
				</div>
				<div id="results" ng-if="clavList.length>0">
					<!-- Infos complementaire -->
					<div class="row">
						<div class="col-xs-6 col-md-6">
							<div class="panel panel-default">
							  <div class="panel-body">
							  	<b>Number of undisplayed values :</b> {{nbUndisplayedValues}}
							  </div>
							</div>
						</div>
						<div class="col-xs-6 col-md-6">
							<div class="panel panel-default">
							  <div class="panel-body">
							  	<b>Number of clipped out rows :</b> {{nbClippedOutRows}}
							  </div>
							</div>
						</div>
					</div>
					<!-- Tableau de resultats -->
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<table class="table table-striped table-hover table-bordered">
								<thead>
									<tr>
										<th>#</th>
										<th>Value</th>
										<th>Count</th>
										<th>Average Age</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="clav in clavList">
										<th scope="row">{{$index+1}}</th>
										<td>{{clav.value}}</td>
										<td>{{clav.count}}</td>
										<td>{{clav.averageAge}}</td>
									</tr>
								</tbody>
						    </table>
					    </div>
				    </div>
			    </div>
			</div>
		</div>
		 	
		<!-- Controller angular -->
		<script>
		function queryCensusLearnController($q, $scope, $http, $log) {
		
			$.testScope = $scope;
			
			// Constantes
			$scope.CODE_NO_ERROR = 0;
			$scope.CODE_ERROR_QUERYING = 1;
			$scope.CODE_ERROR_FORM = 2;
			$scope.NO_ATTRIBUTE_SELECTED = 'NO_ATTRIBUTE_SELECTED';			
			
			/*
			 * Methodes accessibles en internes uniquement
			 */
			
			// Appel au WS pour recuperer le bean permettant de populate le form
			var getFormBeanWS = function (){
				var delay = $q.defer();
				$http({method: 'GET', url: '/formBean'})
				.success(function(datas, status, headers, config) {
					$scope.errorCode = $scope.CODE_NO_ERROR;
					delay.resolve(datas);
				})
				.error(function(data, status, headers, config) {
					$scope.errorCode = $scope.CODE_ERROR_FORM;
					delay.reject('Unable to get attribute list');
				});				
				return delay.promise;
			};
			
			// Appel au WS pour executer une requete sur un attribut
			var queryCensusLearnWS = function(attribute) {
				var delay = $q.defer();
				$http({method: 'GET', url: '/query/'+attribute})
				.success(function(datas, status, headers, config) {
					$scope.errorCode = $scope.CODE_NO_ERROR;
					delay.resolve(datas);
				})
				.error(function(data, status, headers, config) {
					$scope.errorCode = $scope.CODE_ERROR_QUERYING;
					delay.reject('Unable to query');
				});				
				return delay.promise;
			};
			
			// Methode permettant d'initialiser la webapp
			var initView = function() {
				$scope.selectedAttribute = $scope.NO_ATTRIBUTE_SELECTED;
				$scope.attributeList = [];
				$scope.clavList = [];
				$scope.nbUndisplayedValues = 0;
				$scope.nbClippedOutRows = 0;
				
				$scope.errorCode = $scope.CODE_NO_ERROR;
				
				getFormBeanWS()
				.then(function(datas) {
					$scope.attributeList = datas.attributeList;
				}, function(error) {
					$scope.errorCode = $scope.CODE_ERROR_FORM;
					$log.error('Error while loading attribute list', error);
				});
			};
			
			/*
			 * Methodes exposees a la vue
			 */
			
			// Methode appelee par la vue pour executer une requete sur un attribut
			$scope.queryCensusLearn = function(attribute) {
				queryCensusLearnWS(attribute)
				.then(function(datas) {
					$scope.clavList = datas.clavBeanList;
					$scope.nbUndisplayedValues = datas.nbUndisplayedValues;
					$scope.nbClippedOutRows = datas.nbClippedOutRows;
				}, function(error) {
					$log.error('Error while querying census learn', error);
				});
			};
			
			/*
			 * Initialisation de la vue
			 */
			initView();
			
			
		}
		</script>
	</body>
</html>