package com.cunisiainc.dataiku.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cunisiainc.dataiku.Const;
import com.cunisiainc.dataiku.datasource.CensusLearnAttributeValueDAO;
import com.cunisiainc.dataiku.datasource.CensusLearnQueryManager;
import com.cunisiainc.dataiku.model.CensusLearnQueryFormBean;
import com.cunisiainc.dataiku.model.CensusLearnQueryResultBean;
import com.cunisiainc.dataiku.model.CensusLearnAttributeValueBean;

@Controller
@RequestMapping("/")
public class WSQueryCensusLearnController {
    
	/**
	 * WS permettant de query la BDD sur la table passee en parametre
	 * @param columnName
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value="/query/{columnName}", method = RequestMethod.GET)
    @ResponseBody
    public CensusLearnQueryResultBean queryCensusLearn(@PathVariable String columnName) throws Exception {
        List<CensusLearnAttributeValueBean> clavBeanList = CensusLearnAttributeValueDAO.getClavBeanList(columnName);
        int nbDistinctValues = CensusLearnQueryManager.getCensusLearnAttributeDistinctValuesCount(columnName);
        int nbRows = CensusLearnQueryManager.getCensusLearnRowCount();
        int nbUndisplayedValues = clavBeanList.size() == Const.MAX_DISPLAYED_VALUES ? nbDistinctValues - Const.MAX_DISPLAYED_VALUES : 0;
        int nbClippedOutRows = nbRows - clavBeanList.size();
    	CensusLearnQueryResultBean result = new CensusLearnQueryResultBean(clavBeanList, nbUndisplayedValues, nbClippedOutRows);
        return result;
    } 
    
    /**
     * WS permettant de charger le formulaire d'interrogration de la BDD
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/formBean", method = RequestMethod.GET)
    @ResponseBody
    public CensusLearnQueryFormBean getAttributeList() throws Exception {
        List<String> attributeList = CensusLearnQueryManager.getAttributeList();
        CensusLearnQueryFormBean formBean = new CensusLearnQueryFormBean(attributeList);
        return formBean;
    } 
	
}
