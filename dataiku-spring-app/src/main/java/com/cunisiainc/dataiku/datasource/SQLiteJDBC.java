package com.cunisiainc.dataiku.datasource;

import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.log4j.Logger;

public abstract class SQLiteJDBC {

	private static final Logger logger = Logger.getLogger(SQLiteJDBC.class);
	
	/**
	 * Ouvre une connexion � la BDD
	 * @throws Exception 
	 */
	public static Connection openConnection() throws Exception {
		try {
			Connection c = null;
		    
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:/dev-pers/dataiku_db/us-census.db");
			c.setAutoCommit(false);
			logger.info("Opened database successfully");
			
			return c;
		} catch (Exception e) {
			throw new Exception("Erreur lors de la connexion � la BDD", e);
		}
		
	}
	
}
