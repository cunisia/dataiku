package com.cunisiainc.dataiku.datasource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.cunisiainc.dataiku.Const;
import com.cunisiainc.dataiku.model.CensusLearnAttributeValueBean;

public class CensusLearnAttributeValueDAO extends SQLiteJDBC {

	/**
	 * M�thode permettant de query la BDD pour remonter une liste de CensusLearnAttributeValueBean
	 * @param columnName : le nom de la colonne � partir de laquelle on veut construire les CensusLearnAttributeValueBean
	 * @return la liste des 100 premiers CensusLearnAttributeValueBean contruits � partir de la colonne pass�e en param�tre
	 * @throws Exception 
	 */
	public static List<CensusLearnAttributeValueBean> getClavBeanList(String columnName) throws Exception {
		//Constuction de la requete SQL
		String query = "select distinct \""+columnName+"\", count(*) as c, AVG("+CensusLearnAttributeValueBean.AGE_COLUMN_NAME+")"
					+ " from "+CensusLearnAttributeValueBean.TABLE_NAME
					+ " where \"" + columnName + "\" not null"
					+ " group by \""+columnName+"\" order by c desc limit " + Const.MAX_DISPLAYED_VALUES;
		//Ouverture de la connexion
		Connection c = openConnection();
		Statement stmt = c.createStatement();
		//Execution de la requete et mapping des resultats dans CensusLearnAttributeValueBean
		ResultSet rs = stmt.executeQuery(query);
	    List<CensusLearnAttributeValueBean> clavBeanList = new ArrayList<CensusLearnAttributeValueBean>();
	    while (rs.next()) {
	    	CensusLearnAttributeValueBean clavBean = new CensusLearnAttributeValueBean(rs.getString(1), rs.getInt(2), rs.getFloat(3));
	    	clavBeanList.add(clavBean);
	    }
	    //Fermeture des connexions
	    rs.close();
	    stmt.close();
	    c.close();
	    return clavBeanList;
	}
	
}
