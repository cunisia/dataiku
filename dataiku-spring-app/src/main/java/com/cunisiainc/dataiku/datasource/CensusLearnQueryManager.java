package com.cunisiainc.dataiku.datasource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.cunisiainc.dataiku.model.CensusLearnAttributeValueBean;

public class CensusLearnQueryManager extends SQLiteJDBC {

	/**
	 * Methode permettant de recuperer toutes les attributs de la table associee a CensusLearnAttributeValueBean
	 * @return
	 * @throws Exception
	 */
	public static List<String> getAttributeList() throws Exception {
		//Constuction de la requete SQL
		String query = "PRAGMA table_info("+ CensusLearnAttributeValueBean.TABLE_NAME +")";
		//Ouverture de la connexion
		Connection c = openConnection();
		Statement stmt = c.createStatement();
		//Execution de la requete
		ResultSet rs = stmt.executeQuery(query);
	    List<String> attributeList = new ArrayList<String>();
	    while (rs.next()) {
	    	String attributeName = rs.getString(2);
	    	attributeList.add(attributeName);
	    }
	    //Fermeture des connexions
	    rs.close();
	    stmt.close();
	    c.close();
	    Collections.sort(attributeList);
	    return attributeList;
	}
	
	/**
	 * Retourne le nombre de valeurs distinctes pour une colonne donnee dans la table associee au CensusLearnAttributeValueBean
	 * @param columnName : le nom de la colonne pour laquelle on veut connaitre le nombre de valeur distinctes
	 */
	public static int getCensusLearnAttributeDistinctValuesCount(String columnName) throws Exception {
		//Constuction de la requete SQL
		String query = "select count(distinct \""+columnName+"\") from "+CensusLearnAttributeValueBean.TABLE_NAME;
		//Ouverture de la connexion
		Connection c = openConnection();
		Statement stmt = c.createStatement();
		//Execution de la requete
		ResultSet rs = stmt.executeQuery(query);
		int count = 0;
	    if (rs.next()) {
	    	count = rs.getInt(1);
	    }
	    //Fermeture des connexions
	    rs.close();
	    stmt.close();
	    c.close();
	    return count;
	}
	
	/**
	 * Retourn le nombre de lignes totales presentes dans la table associee au CensusLearnAttributeValueBean
	 */
	public static int getCensusLearnRowCount() throws Exception {
		//Constuction de la requete SQL
		String query = "select count(*) from "+CensusLearnAttributeValueBean.TABLE_NAME;
		//Ouverture de la connexion
		Connection c = openConnection();
		Statement stmt = c.createStatement();
		//Execution de la requete
		ResultSet rs = stmt.executeQuery(query);
		int count = 0;
	    if (rs.next()) {
	    	count = rs.getInt(1);
	    }
	    //Fermeture des connexions
	    rs.close();
	    stmt.close();
	    c.close();
	    return count;
	}
	
}
