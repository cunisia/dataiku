package com.cunisiainc.dataiku.model;

/*
 * Cette classe represente un element de la liste renvoyee au client quand ce dernier interroge le serveur pour une colonne donnee
 * (cet element representant une valeur de cette colonne de la table census_lear_sql, accompagne du nombre d'occurences de cette 
 * valeur et de l'age moyen pour lequel elle survient).
 */

public class CensusLearnAttributeValueBean {

	public static final String TABLE_NAME = "census_learn_sql";
	public static final String AGE_COLUMN_NAME = "age";
	
	private String value;
	private int count;
	private float averageAge;
	
	/*
	 * CONSTRUCTEUR
	 */
	
	public CensusLearnAttributeValueBean(String value, int count, float averageAge) {
		this.value = value;
		this.count = count;
		this.averageAge = averageAge;
	}
	
	/*
	 * GETTERs AND SETTERs
	 */
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public float getAverageAge() {
		return averageAge;
	}
	public void setAverageAge(float averageAge) {
		this.averageAge = averageAge;
	}
	
	
	
}
