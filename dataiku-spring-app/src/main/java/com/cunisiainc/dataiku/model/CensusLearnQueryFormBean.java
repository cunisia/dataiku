package com.cunisiainc.dataiku.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/*
 * Cette classe represente le bean envoye � la webapp pour qu'elle puisse populate le formulaire d'interrogation de la table liee a CensusLearnAttributeValuePOJO 
 */

@JsonSerialize
@JsonInclude(Include.NON_NULL)
public class CensusLearnQueryFormBean {

	private List<String> attributeList;

	/*
	 * CONSTRUCTEUR
	 */
	
	public CensusLearnQueryFormBean(List<String> attributeList) {
		this.attributeList = attributeList;
	}
	
	/*
	 * GETTERs AND SETTERs
	 */
	
	public List<String> getAttributeList() {
		return attributeList;
	}

	public void setAttributeList(List<String> attributeList) {
		this.attributeList = attributeList;
	}
	
	
	
}
