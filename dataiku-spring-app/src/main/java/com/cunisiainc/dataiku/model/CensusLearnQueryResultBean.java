package com.cunisiainc.dataiku.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/*
 * Cette classe repr�sente le bean renvoy� lorsque la webapp interroge le serveur pour une colonne donn�e
 */

@JsonSerialize
@JsonInclude(Include.NON_NULL)
public class CensusLearnQueryResultBean {

	private List<CensusLearnAttributeValueBean> clavBeanList;
	private int nbUndisplayedValues;
	private int nbClippedOutRows;
	
	/*
	 * CONSTRUCTEUR
	 */
	
	public CensusLearnQueryResultBean(List<CensusLearnAttributeValueBean> clavPojoList, int nbUndisplayedValues, int nbClippedOutRows) {
		this.clavBeanList = clavPojoList;
		this.nbUndisplayedValues = nbUndisplayedValues;
		this.nbClippedOutRows = nbClippedOutRows;
	}
	
	/*
	 * GETTERs AND SETTERs
	 */
	
	public List<CensusLearnAttributeValueBean> getClavBeanList() {
		return clavBeanList;
	}
	
	public void setClavBeanList(List<CensusLearnAttributeValueBean> clavBeanList) {
		this.clavBeanList = clavBeanList;
	}
	
	public int getNbUndisplayedValues() {
		return nbUndisplayedValues;
	}
	
	public void setNbUndisplayedValues(int nbUndisplayedValues) {
		this.nbUndisplayedValues = nbUndisplayedValues;
	}
	
	public int getNbClippedOutRows() {
		return nbClippedOutRows;
	}
	
	public void setNbClippedOutRows(int nbClippedOutRows) {
		this.nbClippedOutRows = nbClippedOutRows;
	}
	
	
	
}
